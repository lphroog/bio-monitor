package by.rchp.PulseSensorAndroid;

/**
 * Created by Vladimir on 13.04.2015.
 */
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.widget.ArrayAdapter;
import org.achartengine.model.XYSeries;

public class MyFragment extends Fragment {
    public DeviceModuleAdapter fDeviceAdapter = new DeviceModuleAdapter();
    public BluetoothSerialService mSerialService;
    public TPulseDevice fPulseDevice;
    private BluetoothAdapter mBluetoothAdapter = null;
    public static final boolean DEBUG = true;
    public static final String LOG_TAG = "PulseSensor";
   // public XYMultipleSeriesDataset fChartDataset = new XYMultipleSeriesDataset();
    public XYSeries fChartSeries;
    private byte[] myData;
    private ArrayList<Float> pulseData =  new ArrayList<Float>();
    private int dataCount;
    public boolean Started;
    Handler h;
    Timer fTimer = new Timer();
    OnDataPass dataPasser;
    public String filesPath = "";
    // interface to sent data to activity;
    public interface OnDataPass {
        public void onDataPass(ArrayList<Float> data, int count);
        public void onMsgPass(Message msg);
    }
    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        dataPasser = (OnDataPass) a;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        try
        {
                super.onCreate(savedInstanceState);
                if (DEBUG) Log.e(LOG_TAG, "MyFragment -- OnCreate");
                // retain this fragment
                setRetainInstance(true);
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                mSerialService = new BluetoothSerialService(mHandlerBT, fDeviceAdapter);
                mSerialService.setAllowInsecureConnections( true );
                fPulseDevice= new TPulseDevice(fDeviceAdapter);
                fPulseDevice.setDebug(DEBUG);
                fDeviceAdapter.setDebug(DEBUG);
                fPulseDevice.setLogTag(LOG_TAG);
                fDeviceAdapter.setLogTag(LOG_TAG);
                h = new Handler() {
                    public void handleMessage(android.os.Message msg) {
                        try {
                            if (DEBUG) Log.i(LOG_TAG,"MyFragment Handler h -- New Data, size = "+ String.valueOf(pulseData.size())  );
                            if (dataPasser != null) dataPasser.onDataPass(pulseData, dataCount);
                        }
                        catch (Exception e)
                        {
                            if (DEBUG) Log.e(LOG_TAG,"MyFragment Handler h --" + e.getMessage());
                        }
                    };
                };

                fPulseDevice.setOnNewsUpdateListener(
                        new NewsUpdateListener() {
                            @Override
                            public void onNewsUpdate(ArrayList<Float> data, int count) {
                                try {
                                    pulseData = data;
                                    dataCount = count;
                                    h.sendEmptyMessage(0);
                                }
                                catch(Exception e)
                                {
                                    if (DEBUG) Log.e(LOG_TAG, "OnNewsUpdateListener -- " + e.getMessage());
                                }
                            }
                        }
                );

                fTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try{
                            fPulseDevice.service();
                        }
                        catch (Exception e)
                        {
                            if (DEBUG) Log.e(LOG_TAG,"My Fragment timer --" + e.getMessage());
                        }
                    }
                }, 0, 10);
        }catch(Exception e)
        {
            if (DEBUG) Log.e(LOG_TAG, e.getMessage());
        }
    }
    public int getConnectionState() {
        return mSerialService.getState();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSerialService != null)
            mSerialService.stop();
        fTimer.cancel();
        fTimer.purge();
        fTimer = null;
        // store the data in the fragment
        Log.e(LOG_TAG, "+++ MyFragment On Destroy +++");
    }

    private final Handler mHandlerBT = new Handler() {
        public void handleMessage(Message msg) {
            if (dataPasser!=null) dataPasser.onMsgPass(msg);

        }
    };
}
