package by.rchp.PulseSensorAndroid;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import org.achartengine.GraphicalView;
import org.achartengine.ChartFactory;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import android.widget.LinearLayout.LayoutParams;
import java.util.Random;
import  android.graphics.Color;
import org.achartengine.chart.PointStyle;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import  android.app.FragmentManager;
import android.widget.LinearLayout;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.os.AsyncTask;

public class PulseSensorAndroid extends Activity implements OnClickListener,MyFragment.OnDataPass{
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int ORIENTATION_SENSOR    = 0;
	public static final int ORIENTATION_PORTRAIT  = 1;
	public static final int ORIENTATION_LANDSCAPE = 2;
    private static TextView mTitle;
    private String mConnectedDeviceName = null;
    public static final boolean DEBUG = true;
    public static final boolean LOG_CHARACTERS_FLAG = DEBUG && false;
    public static final boolean LOG_UNKNOWN_ESCAPE_SEQUENCES = DEBUG && false;
	public static final String LOG_TAG = "PulseSensor";
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;	
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
	private static InputMethodManager mInputManager;
	private boolean mEnablingBT;
    private int mFontSize = 9;
    private int mColorId = 2;
    private GraphicalView mChartView;
    private int mScreenOrientation = 0;
    private static final String FONTSIZE_KEY = "fontsize";
    private static final String COLOR_KEY = "color";
    private static final String SCREENORIENTATION_KEY = "screenorientation";
    public static final int WHITE = 0xffffffff;
    public static final int BLACK = 0xff000000;
    public static final int BLUE = 0xff344ebd;
    private static final int[][] COLOR_SCHEMES = {{BLACK, WHITE}, {WHITE, BLACK}, {WHITE, BLUE}};
    private SharedPreferences mPrefs;
    private MenuItem mMenuItemConnect;
    private static final int SERIES_NR = 1;
    private Dialog         mAboutDialog;
    private String[] mMenuText;
    private String[] mMenuSummary;
    private int CurrentRowToRead=1;
    String m_chosen="";
    boolean PulseStarted=false;
    // view elements
    Button BtnStartStop;
    private BluetoothAdapter mBluetoothAdapter = null;
    //public  XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    public XYSeries fChartSeries;
    MyFragment dataFragment;
    String filesPath="";
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        try {
            if (DEBUG) Log.e(LOG_TAG, "+++ ON CREATE +++");
            mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            readPrefs();
            //
            filesPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/biomonitor/";
            File folder = new File(filesPath);
            if (!folder.exists() || !folder.isDirectory()) folder.mkdir();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String currentDateTimeString = format.format(new Date());
            filesPath+="pulse_Data" + currentDateTimeString + ".txt";
            //
            mInputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            // Set up the window layout
            requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
            setContentView(R.layout.main);
            // Check if Bluetooth available
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                if (DEBUG) Log.e(LOG_TAG, "+++ No Bluetooth +++");
                finishDialogNoBluetooth();
                return;
            }
            // start activity
            setContentView(R.layout.term_activity);
            ActionBar actionBar = this.getActionBar();
            actionBar.hide();
            // try to restore data
            FragmentManager fm = getFragmentManager();
            dataFragment = (MyFragment) fm.findFragmentByTag("data");
            if (dataFragment == null) {
                // add the fragment
                dataFragment = new MyFragment();
                fm.beginTransaction().add(dataFragment, "data").commit();
                // create new dataset and one pulse array
                float[] data = {1,3,20,60,85,100,105,100,85,60,20,12,30,45,43,30,20,10,1};
                ArrayList<Float> tmpData = new ArrayList<Float>();
                for (int i=0;i<data.length;i++) tmpData.add(data[i]);
                fChartSeries = new XYSeries("Pulse");
                DrawChart(tmpData, tmpData.size());
            }
            else
            {
                fChartSeries = dataFragment.fChartSeries;
                PulseStarted = dataFragment.Started;
                filesPath = dataFragment.filesPath;
                DrawChart(new ArrayList<Float>(),0);
            }
            BtnStartStop = (Button) findViewById(R.id.button);
            BtnStartStop.setOnClickListener(this);
            if (PulseStarted) BtnStartStop.setText(R.string.Stop_measure);
            else BtnStartStop.setText(R.string.Start_measure);
            if (DEBUG) Log.e(LOG_TAG, "+++ DONE IN ON CREATE +++");
        }catch(Exception e)
        {
            if (DEBUG) Log.e(LOG_TAG, e.getMessage());
        }
	}
    public void onDataPass(final ArrayList<Float> data, final int count)
    {
        try {
            if (DEBUG) Log.e(LOG_TAG,"onDataPass, Count = " +String.valueOf(count));
            DrawChart(data, count);
            String dataString ="";
            if ((count >0) && (data.size() > 0) && (count<=data.size()))
            {
                int size = data.size();
                for (int k = 0; k < count; k++) dataString += String.valueOf(data.get(size - count + k)) + "\r\n";
                new MyTask().execute(dataString);
            }

        }
        catch(Exception e)
        {
            if (DEBUG) Log.e(LOG_TAG,"onDataPass - Error = " + e.getMessage());
        }
    }
    public void onMsgPass(Message msg)
    {
        try {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (DEBUG) Log.i(LOG_TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothSerialService.STATE_CONNECTED:
                            if (mMenuItemConnect != null) {
                                mMenuItemConnect.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
                                mMenuItemConnect.setTitle(R.string.disconnect);
                            }
                            break;

                        case BluetoothSerialService.STATE_CONNECTING:
                            // mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothSerialService.STATE_LISTEN:
                        case BluetoothSerialService.STATE_NONE:
                            if (mMenuItemConnect != null) {
                                mMenuItemConnect.setIcon(android.R.drawable.ic_menu_search);
                                mMenuItemConnect.setTitle(R.string.connect);
                            }
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_connected_to) + " "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        catch (Exception e)
        {
            if (DEBUG) Log.e(LOG_TAG,e.getMessage());
        }
    }
    class MyTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected Void doInBackground(String... mydata) {
            try {
                writeLog(mydata[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        public boolean writeLog(String astring) {
            String state = Environment.getExternalStorageState();
            File logFile = new File(filesPath);
            if (Environment.MEDIA_MOUNTED.equals(state)) {

                try {
                    FileOutputStream f = new FileOutputStream( logFile, true );
                    PrintWriter pw = new PrintWriter(f);
                    pw.print(astring);
                    pw.flush();
                    pw.close();
                    f.close();
                    //if (DEBUG) Log.e(LOG_TAG, "WROTE TO FILE SUCCESFULLY");
                } catch (IOException e) {
                    if (DEBUG) Log.e(LOG_TAG, "ERROR OCCURE WHILE WRITING TO FILE");
                    e.printStackTrace();
                }
            }
            return true;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }



   @Override
    public void onClick(View v) {
        // по id определеяем кнопку, вызвавшую этот обработчик
        String filename="";
        switch (v.getId()) {
            case (R.id.button):
                byte[] data;
                BtnStartStop = (Button) findViewById(R.id.button);

                if (!PulseStarted)//Start
                {
                    data = DeviceModuleAdapter.GetStartMeasureArray(DeviceModuleAdapter.StartMeasure);
                    BtnStartStop.setText(R.string.Stop_measure);
                    PulseStarted=true;
                }
                else{
                    data = DeviceModuleAdapter.GetStopMeasureArray();
                    PulseStarted=false;
                    BtnStartStop.setText(R.string.Start_measure);
                }
                send(data);
               // fPulseDevice.write(data, data.length);
                break;
        }
    }
	@Override
	public void onStart() {
		super.onStart();
		if (DEBUG)
			Log.e(LOG_TAG, "++ ON START ++");
		mEnablingBT = false;
	}

	@Override
	public synchronized void onResume() {
		super.onResume();

		if (DEBUG) {
			Log.i(LOG_TAG, "+ ON RESUME +");
		}

		if (!mEnablingBT) { // If we are turning on the BT we cannot check if it's enable
		    if ( (mBluetoothAdapter != null)  && (!mBluetoothAdapter.isEnabled()) ) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.alert_dialog_turn_on_bt)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.alert_dialog_warning_title)
                    .setCancelable( false )
                    .setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
                    	public void onClick(DialogInterface dialog, int id) {
                    		mEnablingBT = true;
                    		Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    		startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                    	}
                    })
                    .setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
                    	public void onClick(DialogInterface dialog, int id) {
                    		finishDialogNoBluetooth();
                    	}
                    });
                AlertDialog alert = builder.create();
                alert.show();
		    }
		    if (mBluetoothAdapter != null) {
		    	readPrefs();
		    	updatePrefs();
		    }

        }
	}
    private void DrawChart(ArrayList<Float> data, int count)
    {
        try {

            if ((count >0) && (data.size() > 0) && (count<=data.size())) {
                int size = data.size();
                if (DEBUG) Log.i(LOG_TAG, "Draw Chart" + String.valueOf(count));
                fChartSeries.clear();
                for (int k = 0; k < count; k++) fChartSeries.add(k, data.get(size - count + k));
            }
            if (mChartView == null) {
                XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
                dataset.addSeries(fChartSeries);
                LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
                mChartView = ChartFactory.getLineChartView(this, dataset, getDemoRenderer());
                layout.addView(mChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
            } else {
                mChartView.repaint();
            }
        }catch (Exception e)
        {
            if (DEBUG) Log.e(LOG_TAG, "Draw Chart" + e.getMessage());
        }
    }
    private XYMultipleSeriesRenderer getDemoRenderer() {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(16);
        renderer.setChartTitleTextSize(20);
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setPointSize(1f);
        renderer.setMargins(new int[] {20, 30, 15, 0});
        XYSeriesRenderer r = new XYSeriesRenderer();
        r.setColor(Color.BLUE);
        r.setPointStyle(PointStyle.SQUARE);
        r.setFillBelowLine(true);
        r.setFillBelowLineColor(Color.WHITE);
        r.setFillPoints(true);
        renderer.addSeriesRenderer(r);

        renderer.setAxesColor(Color.DKGRAY);
        renderer.setLabelsColor(Color.LTGRAY);
        return renderer;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (DEBUG)
            Log.e(LOG_TAG, "- ON CONFIG CHANGED -");
    }

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (DEBUG)
			Log.e(LOG_TAG, "- ON PAUSE -");
		}

    @Override
    public void onStop() {
        super.onStop();
        if(DEBUG)
        	Log.e(LOG_TAG, "-- ON STOP --");
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
        FragmentManager fm = getFragmentManager();
        dataFragment = (MyFragment) fm.findFragmentByTag("data");
        if (dataFragment != null) {
            // add the fragment
            dataFragment.fChartSeries = fChartSeries;
            dataFragment.Started = PulseStarted;
            dataFragment.filesPath = filesPath;
        }
		if (DEBUG)
			Log.e(LOG_TAG, "--- ON DESTROY ---");
	}
   
    private void readPrefs() {

        if (DEBUG)
            Log.e(LOG_TAG, "- ON READ PREFS -");
        mFontSize = readIntPref(FONTSIZE_KEY, mFontSize, 20);
        mColorId = readIntPref(COLOR_KEY, mColorId, COLOR_SCHEMES.length - 1);
		mScreenOrientation = readIntPref(SCREENORIENTATION_KEY, mScreenOrientation, 2);
    }

    private void updatePrefs() {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON UPDATE PREFS -");
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        setColors();
        switch (mScreenOrientation) {
		case ORIENTATION_PORTRAIT:
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			break;
		case ORIENTATION_LANDSCAPE:
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			break;
		default:
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
    }

    private int readIntPref(String key, int defaultValue, int maxValue) {

        int val;
        try {
            val = Integer.parseInt( mPrefs.getString(key, Integer.toString(defaultValue)) );
        } catch (NumberFormatException e) {
            val = defaultValue;
        }
        val = Math.max(0, Math.min(val, maxValue));
        return val;
    }
    
	public int getConnectionState() {
		return dataFragment.mSerialService.getState();
	}

	public void send(byte[] out) {
    	if ( out.length > 0 ) {
            dataFragment.mSerialService.write( out );
    	}
    }

    public void toggleKeyboard() {
  		mInputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public int getTitleHeight() {
    	return mTitle.getHeight();
    }
    
    public void finishDialogNoBluetooth() {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON FINISH DIALOG NO BLUETOOTH CHANGED -");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.alert_dialog_no_bt)
        .setIcon(android.R.drawable.ic_dialog_info)
        .setTitle(R.string.app_name)
        .setCancelable( false )
        .setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       finish();            	
                   }
               });
        AlertDialog alert = builder.create();
        alert.show(); 
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(DEBUG) Log.d(LOG_TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE:

            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Get the BLuetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                dataFragment.mSerialService.connect(device);
            }
            break;

        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode != Activity.RESULT_OK) {
                Log.d(LOG_TAG, "BT not enabled");
                
                finishDialogNoBluetooth();                
            }
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON KEY UP -");
        if (isSystemKey(keyCode, event)) {
            // Don't intercept the system keys
            return super.onKeyDown(keyCode, event);
        }
        return true;

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON KEY DOWN -");
        if (isSystemKey(keyCode, event)) {
            // Don't intercept the system keys
            return super.onKeyUp(keyCode, event);
        }
        return true;

    }
    private boolean isSystemKey(int keyCode, KeyEvent event) {
        return event.isSystem();
    }
    private boolean handleControlKey(int keyCode, boolean down) {

        return false;
    }


    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON CREATE OPTIONS MENU -");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        mMenuItemConnect = menu.getItem(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON OPTIONS ITEM SELECTED -");
        switch (item.getItemId()) {
            case R.id.connect:

                if (getConnectionState() == BluetoothSerialService.STATE_NONE) {
                    // Launch the DeviceListActivity to see devices and do scan
                    Intent serverIntent = new Intent(this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                } else if (getConnectionState() == BluetoothSerialService.STATE_CONNECTED) {
                    dataFragment.mSerialService.stop();
                    dataFragment.mSerialService.start();
                }
                return true;
            case R.id.preferences:
                doPreferences();
                return true;

            case R.id.menu_about:
                showAboutDialog();
                return true;
        }
        return false;
    }

    private void doPreferences() {
        if (DEBUG)
            Log.e(LOG_TAG, "- DO PREFERENCES  -");
        startActivity(new Intent(this, TermPreferences.class));
    }
    
    public void doOpenOptionsMenu() {
        if (DEBUG)
            Log.e(LOG_TAG, "- ON OPEN OPTIONS MENU -");
        openOptionsMenu();
    }

    private void setColors() {
        int[] scheme = COLOR_SCHEMES[mColorId];
      //  mEmulatorView.setColors(scheme[0], scheme[1]);
    }

	private void showAboutDialog() {
		mAboutDialog = new Dialog(PulseSensorAndroid.this);
		mAboutDialog.setContentView(R.layout.about);
		mAboutDialog.setTitle( getString( R.string.app_name ) + " " + getString( R.string.app_version ));
		
		Button buttonOpen = (Button) mAboutDialog.findViewById(R.id.buttonDialog);
		buttonOpen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		 
				mAboutDialog.dismiss();
			}
		});		
		
		mAboutDialog.show();
    }
}





/**
 * A multi-thread-safe produce-consumer byte array.
 * Only allows one producer and one consumer.
 */

class ByteQueue {
    public ByteQueue(int size) {
        mBuffer = new byte[size];
    }

    public int getBytesAvailable() {
        synchronized(this) {
            return mStoredBytes;
        }
    }

    public int read(byte[] buffer, int offset, int length)
        throws InterruptedException {
        if (length + offset > buffer.length) {
            throw
                new IllegalArgumentException("length + offset > buffer.length");
        }
        if (length < 0) {
            throw
            new IllegalArgumentException("length < 0");

        }
        if (length == 0) {
            return 0;
        }
        synchronized(this) {
            while (mStoredBytes == 0) {
                wait();
            }
            int totalRead = 0;
            int bufferLength = mBuffer.length;
            boolean wasFull = bufferLength == mStoredBytes;
            while (length > 0 && mStoredBytes > 0) {
                int oneRun = Math.min(bufferLength - mHead, mStoredBytes);
                int bytesToCopy = Math.min(length, oneRun);
                System.arraycopy(mBuffer, mHead, buffer, offset, bytesToCopy);
                mHead += bytesToCopy;
                if (mHead >= bufferLength) {
                    mHead = 0;
                }
                mStoredBytes -= bytesToCopy;
                length -= bytesToCopy;
                offset += bytesToCopy;
                totalRead += bytesToCopy;
            }
            if (wasFull) {
                notify();
            }
            return totalRead;
        }
    }

    public void write(byte[] buffer, int offset, int length)
    throws InterruptedException {
        if (length + offset > buffer.length) {
            throw
                new IllegalArgumentException("length + offset > buffer.length");
        }
        if (length < 0) {
            throw
            new IllegalArgumentException("length < 0");

        }
        if (length == 0) {
            return;
        }
        synchronized(this) {
            int bufferLength = mBuffer.length;
            boolean wasEmpty = mStoredBytes == 0;
            while (length > 0) {
                while(bufferLength == mStoredBytes) {
                    wait();
                }
                int tail = mHead + mStoredBytes;
                int oneRun;
                if (tail >= bufferLength) {
                    tail = tail - bufferLength;
                    oneRun = mHead - tail;
                } else {
                    oneRun = bufferLength - tail;
                }
                int bytesToCopy = Math.min(oneRun, length);
                System.arraycopy(buffer, offset, mBuffer, tail, bytesToCopy);
                offset += bytesToCopy;
                mStoredBytes += bytesToCopy;
                length -= bytesToCopy;
            }
            if (wasEmpty) {
                notify();
            }
        }
    }
    private byte[] mBuffer;
    private int mHead;
    private int mStoredBytes;
}




