package by.rchp.PulseSensorAndroid;
import android.util.Log;

import java.util.ArrayList;
/**
 * Created by Vladimir on 02.04.2015.
 */
public class TPulseDevice {
    private DeviceModuleAdapter fAdapter;
    ArrayList<NewsUpdateListener> listeners = new ArrayList<NewsUpdateListener> ();
    arrayListContainer Temp = new arrayListContainer();
    public ArrayList<Float> fRealization = new ArrayList<Float>();
    public int Counter=0;
    public static final int DataCount = 1024;
    public static final int BufferSize =10;
    private static boolean DEBUG = true;
    public static String LOG_TAG = "PulseSensor";
    public TPulseDevice(DeviceModuleAdapter Adapter)
    {
        fAdapter = Adapter;
    }

    public void setOnNewsUpdateListener (NewsUpdateListener listener)
    {
        // Store the listener object
        this.listeners.add(listener);
    }
    public void write(byte[] buffer, int length)
    {
       // fAdapter.write(buffer,length);
    }
    public void setDebug(boolean aDebug)
    {
        DEBUG = aDebug;
    }
    public void setLogTag(String atag)
    {
        LOG_TAG = atag;
    }
    public void service()
    {
        //if (DEBUG) Log.e(LOG_TAG, "TPulseDevice - Service()");
        byte code = DeviceModuleAdapter.FuncCodeToDataCode[DeviceModuleAdapter.PulseWave];
        while( fAdapter.getData(Temp, code))
        {
            if (Temp.IntList.size()==BufferSize*2)// 10 words = 20 bytes
            {
                fRealization.addAll(getSample(Temp.IntList));
                Counter+=BufferSize;
                if (Counter>=DataCount)
                {
                    if (DEBUG) Log.i(LOG_TAG, "TPulseDevice - Service()- fSubRealization.size()>1024");
                    //send data to main class]

                    for (int k=0;k<listeners.size();k++ )
                    {
                        listeners.get(k).onNewsUpdate(fRealization, DataCount);
                    }
                    Counter=0;
                    break;
                }
            }
        }
    }
    private ArrayList<Float> getSample(ArrayList<Integer> packet)
    {
        ArrayList<Float> result=new ArrayList<Float>();
        int tmp=0;
        for(int i=0;i<BufferSize;i++) {
            tmp = packet.get(i*2+1) + packet.get(i*2)*256;
            result.add((float)tmp);
        }
        return result;
    }
}
