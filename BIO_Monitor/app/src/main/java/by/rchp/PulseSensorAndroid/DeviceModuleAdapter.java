package by.rchp.PulseSensorAndroid;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Vladimir on 02.04.2015.
 */
interface NewsUpdateListener
{
    public void onNewsUpdate(ArrayList<Float> data, int count);
}
public class DeviceModuleAdapter {
    public static final byte fStartSymbol = 0x3A;
    public static final byte fStopSymbol1 = 0x0D;
    public static final byte fStopSymbol2 = 0x0A;
    public static final byte StartMeasure=0x01;
    public static final byte StopMeasure =0x02;
    public static final byte IdentifyDevice =0x03;
    public static final byte StartMeasureAcceleration=0x05;
    public static final byte StartMeasureGyro =0x06;
    public static final byte SetRange =0x07;
    public static final byte SetMode =0x08;
    public static final byte Acceleration = 0x01;
    public static final byte Gyro = 0x02;
    public static final byte PulseWave = 0x03;
    public static final byte BreatheWave = 0x04;
    public static final byte Temperature = 0x05;
    public static final byte SkinResponse = 0x06;
    public static final byte VibroTrainer = 0x07;
    public static final int threshold=100;
    public static boolean DEBUG = false;
    private arrayListContainer TempDataBuff = new arrayListContainer();
    private ByteQueue InQueue;
    public static final int UPDATE = 1;
    private byte[] mReceiveBuffer;
    public static String LOG_TAG = "PulseSensor";
    public ArrayList<ArrayList<Integer>> packets = new ArrayList<ArrayList<Integer>>();

    public DeviceModuleAdapter()
    {
        init();
    }
    private final Handler mHandler = new Handler() {
        /**
         * Handle the callback message. Call our enclosing class's update
         * method.
         *
         * @param msg The callback message.
         */
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == UPDATE) {
                update();
            }
        }
    };
    public void init () {
        mReceiveBuffer = new byte[4 * 1024];
        InQueue = new ByteQueue(4 * 1024);
    }
    public void setDebug(boolean aDebug)
    {
        DEBUG = aDebug;
    }
    public void setLogTag(String atag)
    {
        LOG_TAG = atag;
    }
    public static byte[] AsciiTable = //new byte[] - !! Лишнее
            {
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //47
                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,											//57
                    0, 0, 0, 0, 0, 0, 0,													//64
                    10,	11,	12,	13,	14,	15
            };
    public static byte[] ByteToAscii = //new byte[]  - !! Лишнее
            {
                    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
                    0x41, 0x42, 0x43, 0x44, 0x45, 0x46
            };
    public static byte[] FuncCodeToDataCode = //new byte[]  - !! Лишнее
            {
                    0x00, 0x05, 0x06, 0x07
            };
    public static byte[] GetStartMeasureArray(byte aCode)// aCode = PCtoSensorCodes.StartMeasure или StartMeasureAcceleration или StartMeasureGyro
    {
        byte[] res = new byte[4];
        res[0] = fStartSymbol;
        res[1] = ByteToAscii[(byte)aCode >> 4];
        res[2] = ByteToAscii[(byte)aCode & 0x0F];
        res[3] = fStopSymbol1;
        return res;
    }
    public static byte[] GetStopMeasureArray()
    {
        byte[] res = new byte[4];
        res[0] = fStartSymbol;
        res[1] = ByteToAscii[StopMeasure >> 4];
        res[2] = ByteToAscii[StopMeasure & 0x0F];
        res[3] = fStopSymbol1;
        return res;
    }

    public static byte[] GetIdentifyArray()
    {
        byte[] res = new byte[4];
        res[0] = fStartSymbol;
        res[1] = ByteToAscii[IdentifyDevice >> 4];
        res[2] = ByteToAscii[IdentifyDevice & 0x0F];
        res[3] = fStopSymbol1;
        return res;
    }
    public static ArrayList<Integer> AsciiArrayToByte(ArrayList<Byte> InData, int StartInd, int count)
    {
        ArrayList<Integer> result = new ArrayList<Integer>();
        if ((count % 2 > 0) || InData.size() < count) return result;
        for (int i = 0; i < count / 2; i++)
        {
            int val =  (int)(AsciiTable[InData.get(i * 2 + 1 + StartInd)]);
            val |= (int)(AsciiTable[InData.get(i * 2 + StartInd)] << 4);
            result.add( val);
        }
        return result;
    }
    public void AddData(byte[] data, int acount)
    {
      for (int i=0;i<acount;i++) TempDataBuff.byteList.add(data[i]);
    }
    public int GetFirstValidData(arrayListContainer OutData)
    {
       // if (DEBUG) Log.e(LOG_TAG, "DeviceAdapter - GetFirstValidData()");
        ArrayList<Byte> Data = new ArrayList<Byte>();
        if (TempDataBuff.byteList.size() >= threshold){										// чтобы не проверять маленькие пакеты данных
            boolean startFound = false;
            boolean endFound = false;
            byte read;
            byte ind = 0;
            while (!endFound) {
                if (TempDataBuff.byteList.size()==0) return -1;
                read = (byte)TempDataBuff.byteList.get(0);
                TempDataBuff.byteList.remove(0);
                if (startFound) {
                    if (read == fStopSymbol1) endFound = true;
                    else {
                        Data.add(read);
                        ind++;
                    }
                }
                if (read == fStartSymbol) startFound = true;
            }
            if (ind>0) OutData.IntList = AsciiArrayToByte(Data,0,ind);
            return OutData.IntList.get(0);
        }
        return -1;
    }
    public void write(byte[] buffer, int length) {
        //if (DEBUG) Log.e(LOG_TAG, "DeviceAdapter - write()");

        try {
            InQueue.write(buffer, 0, length);
        } catch (InterruptedException e) {
            if (DEBUG) Log.e(LOG_TAG, "DeviceAdapter - write() exception" + e.getMessage());
        }
        mHandler.sendMessage( mHandler.obtainMessage(UPDATE));
    }

    /**
     * Look for new input from the ptty, send it to the terminal emulator.
     */
    private void update() {
       // if (DEBUG) Log.e(LOG_TAG, "DeviceAdapter - update() started");
        byte LFCRdetected=0;
        int i=0, j=0;
        int datatype=0; //0 - digits, 1 - strings, string const{}
        int bytesAvailable = InQueue.getBytesAvailable();
        int bytesToRead = Math.min(bytesAvailable, mReceiveBuffer.length);
        try {
            int bytesRead = InQueue.read(mReceiveBuffer, 0, bytesToRead);// TSV code
            AddData(mReceiveBuffer,bytesRead);
            arrayListContainer data = new arrayListContainer();
            while (GetFirstValidData(data)>=0)
            {
                packets.add(data.IntList);
            }
        }
        catch(InterruptedException e)
        {
            if (DEBUG) Log.e(LOG_TAG,"DeviceAdapter - update() exception" + e.getMessage());
        }
   }
   public boolean getData(arrayListContainer data,byte opcode)
   {
      // if (DEBUG) Log.e(LOG_TAG,"DeviceAdapter - getData()" );
       for(int i=0;i<packets.size();i++) if (packets.get(i).get(0)==opcode)
           {
               data.IntList = packets.get(i);
               data.IntList.remove(0);
               packets.remove(i);
               return true;
           }
       return false;
   }

}
